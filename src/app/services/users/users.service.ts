import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {User} from '../../models/users';
import {USERS} from '../../models/mock-data/users-mock';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() {
  }

  getUsers(): Observable<User[]> {
    /* You must use the real get request not the mock
     return of(this.http
        .get<User[]>
        .map(data => _.values(data))
        .do(console.log));
    */
    return of(USERS);
  }

  getUser(id: number): Observable<User> {
    return of(USERS.find(user => user.id === id));
  }

  addUser(id: number, name: string) {
    USERS.push({id: id, name: name});
  }

  deleteUser(id: number) {
    // TODO continue the delete
  }
}
