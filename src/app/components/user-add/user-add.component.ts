import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/users/users.service';
import {User} from '../../models/users';
import {MessageService} from '../../services/message/message.service';

declare var $: any;


@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  user: User;

  constructor(
    private usersService: UsersService,
    private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.resetUser();
  }

  resetUser(): void {
    this.user = new User(0, '');
  }

  addUser(): void {
    $('#modalAddUser').modal('hide');
    this.usersService.addUser(this.user.id, this.user.name);
    this.messageService.add('User ' + this.user.name + ' Added');
    this.resetUser();
  }
}
