import {Component, OnInit} from '@angular/core';
import {User} from '../../models/users';
import {UsersService} from '../../services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  selectedUser: User;
  users: User[];

  constructor(private user: UsersService) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.user.getUsers()
      .subscribe(users => this.users = users);
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

}
