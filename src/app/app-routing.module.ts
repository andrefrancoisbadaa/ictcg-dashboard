import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ConnexionComponent} from './components/connexion/connexion.component';
import {UsersComponent} from './components/users/users.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {UserAddComponent} from './components/user-add/user-add.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'detail/:id',
    component: UserDetailComponent
  },

  {
    path: 'user/add',
    component: UserAddComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'connexion',
    component: ConnexionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
