import { User } from '../users';

export let USERS: User[] = [
  { id: 11, name: 'François André' },
  { id: 12, name: 'François Fynck' },
  { id: 17, name: 'Etienne' },
  { id: 13, name: 'test1' },
  { id: 14, name: 'test2' },
  { id: 15, name: 'test3' },
  { id: 16, name: 'test4' }
];
